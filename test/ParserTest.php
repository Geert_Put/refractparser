<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Test;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\LinkElement;
use Phpro\RefractParser\Element\OptionElement;
use Phpro\RefractParser\Element\Primitive\ArrayElement;
use Phpro\RefractParser\Element\Primitive\BooleanElement;
use Phpro\RefractParser\Element\ExtendElement;
use Phpro\RefractParser\Element\Primitive\MemberElement;
use Phpro\RefractParser\Element\Primitive\NullElement;
use Phpro\RefractParser\Element\Primitive\NumberElement;
use Phpro\RefractParser\Element\Primitive\ObjectElement;
use Phpro\RefractParser\Element\Primitive\StringElement;
use Phpro\RefractParser\Element\RefElement;
use Phpro\RefractParser\Element\SelectElement;
use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Parser;
use PHPUnit\Framework\TestCase;

/**
 * Class ParserTest
 *
 * @package Phpro\RefractParser\Test
 */
final class ParserTest extends TestCase
{

    public function testParseThrowsExceptionOnJsonDecodeError()
    {
        $parser = new Parser();

        $this->expectException(ParserException::class);
        $parser->parse('Certainly not valid JSON');
    }

    public function testParseNullElement()
    {
        $jsonString = <<<'EOL'
{
  "element": "null",
  "content": null
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(NullElement::class, $result);
        $this->assertEquals('null', $result->getElement());
        $this->assertEquals(null, $result->getContent()->getValue());
    }

    public function testParseStringElement()
    {
        $jsonString = <<<'EOL'
{
  "element": "string",
  "content": "foobar"
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(StringElement::class, $result);
        $this->assertEquals('string', $result->getElement());
        $this->assertInternalType('string', $result->getContent()->getValue());
        $this->assertEquals('foobar', $result->getContent()->getValue());
    }

    public function testParseNumberElementInt()
    {
        $jsonString = <<<'EOL'
{
  "element": "number",
  "content": 400
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(NumberElement::class, $result);
        $this->assertEquals('number', $result->getElement());
        $this->assertInternalType('int', $result->getContent()->getValue());
        $this->assertEquals(400, $result->getContent()->getValue());
    }

    public function testParseNumberElementFloat()
    {
        $jsonString = <<<'EOL'
{
  "element": "number",
  "content": 400.666
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(NumberElement::class, $result);
        $this->assertEquals('number', $result->getElement());
        $this->assertInternalType('float', $result->getContent()->getValue());
        $this->assertEquals(400.666, $result->getContent()->getValue());
    }

    public function testParseBooleanElement()
    {
        $jsonString = <<<'EOL'
{
  "element": "boolean",
  "content": true
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(BooleanElement::class, $result);
        $this->assertEquals('boolean', $result->getElement());
        $this->assertInternalType('boolean', $result->getContent()->getValue());
        $this->assertEquals(true, $result->getContent()->getValue());
    }

    public function testParseArrayElement()
    {
        $jsonString = <<<'EOL'
{
  "element": "array",
  "content": [
    {
      "element": "string",
      "content": "foo"
    },
    {
      "element": "number",
      "content": 400
    },
    {
      "element": "boolean",
      "content": true
    }
  ]
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(ArrayElement::class, $result);
        $this->assertEquals('array', $result->getElement());
        $this->assertInternalType('array', $result->getContent()->getValue());
        foreach ($result->getContent()->getValue() as $element) {
            $this->assertInstanceOf(BaseElement::class, $element);
        }
    }

    // Todo : add additional tests for all possible content types
    public function testParseObjectElement()
    {
        $jsonString = <<<'EOL'
{
  "element": "object",
  "content": [
    {
      "element": "member",
      "content": {
        "key": {
          "element": "string",
          "content": "foo"
        },
        "value": {
          "element": "string",
          "content": "bar"
        }
      }
    }
  ]
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(ObjectElement::class, $result);
        $this->assertEquals('object', $result->getElement());
        $this->assertInternalType('array', $result->getContent()->getValue());
        foreach ($result->getContent()->getValue() as $element) {
            $this->assertInstanceOf(MemberElement::class, $element);
        }
    }

    public function testParseMemberElement()
    {
        $jsonString = <<<'EOL'
{
  "element": "member",
  "content": {
    "key": {
      "element": "string",
      "content": "foo"
    },
    "value": {
      "element": "string",
      "content": "bar"
    }
  }
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(MemberElement::class, $result);
        $this->assertEquals('member', $result->getElement());
        $this->assertInternalType('object', $result->getContent()->getValue());
        $this->assertNotNull($result->getContent()->getValue()->key);
        $this->assertInstanceOf(StringElement::class, $result->getContent()->getValue()->key);
        $this->assertNotNull($result->getContent()->getValue()->value);
        $this->assertInstanceOf(StringElement::class, $result->getContent()->getValue()->value);
    }

    public function testParseRefElement()
    {
        $jsonString = <<<'EOL'
{
  "element": "ref",
  "content": "http://example.com/document#foo"
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(RefElement::class, $result);
        $this->assertEquals('ref', $result->getElement());
        $this->assertInternalType('string', $result->getContent()->getValue());
        $this->assertEquals('http://example.com/document#foo', $result->getContent()->getValue());
    }

    public function testParseLinkElement()
    {
        $jsonString = <<<'EOL'
        {
          "element": "link",
          "attributes": {
            "relation": {
              "element": "string",
              "content": "profile"
            },
            "href": {
              "element": "string",
              "content": "http://example.com/extensions/info/"
            }
          }
        }
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(LinkElement::class, $result);
        $this->assertEquals('link', $result->getElement());
        $this->assertInstanceOf(StringElement::class, $result->getAttributes()->getAttribute('relation'));
        $this->assertInstanceOf(StringElement::class, $result->getAttributes()->getAttribute('href'));
    }

    public function testParseExtendElement()
    {
        $jsonString = <<<'EOL'
{
  "element": "extend",
  "content": [
    {
      "element": "foo",
      "attributes": {
        "baz": {
          "element": "string",
          "content": "bar"
        }
      },
      "content": "first"
    },
    {
      "element": "foo",
      "content": "second"
    }
  ]
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(ExtendElement::class, $result);
        $this->assertEquals('extend', $result->getElement());
        $this->assertInternalType('array', $result->getContent()->getValue());
        foreach ($result->getContent()->getValue() as $element) {
            $this->assertInstanceOf(BaseElement::class, $element);
        }
    }

    public function testParseOptionElement()
    {
        $jsonString = <<<'EOL'
{
  "element": "option",
  "content": [
    {
      "element": "string",
      "content": "Option 1"
    },
    {
      "element": "string",
      "content": "Option 2"
    },
    {
      "element": "string",
      "content": "Option 3"
    }
  ]
}
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(OptionElement::class, $result);
        $this->assertEquals('option', $result->getElement());
        $this->assertInternalType('array', $result->getContent()->getValue());
        foreach ($result->getContent()->getValue() as $element) {
            $this->assertInstanceOf(BaseElement::class, $element);
        }
    }

    public function testParseSelectElement()
    {
        $jsonString = <<<'EOL'
{
      "element": "select",
      "content": [
        {
          "element": "option",
          "content": [
            {
              "element": "member",
              "content": {
                "key": {
                  "element": "string",
                  "content": "firstName"
                },
                "value": {
                  "element": "string",
                  "content": "John"
                }
              }
            }
          ]
        },
        {
          "element": "option",
          "content": [
            {
              "element": "member",
              "content": {
                "key": {
                  "element": "string",
                  "content": "givenName"
                },
                "value": {
                  "element": "string",
                  "content": "John"
                }
              }
            }
          ]
        }
      ]
    }
EOL;
        $parser = new Parser();
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(SelectElement::class, $result);
        $this->assertEquals('select', $result->getElement());
        $this->assertInternalType('array', $result->getContent()->getValue());
        foreach ($result->getContent()->getValue() as $element) {
            $this->assertInstanceOf(OptionElement::class, $element);
        }
    }

    public function testParseCustomElement()
    {
        $jsonString = <<<'EOL'
{
  "element": "myCustomElement",
  "content": "My custom content"
}
EOL;
        $parser = new Parser([
            [
                'className' => StringElement::class,
                'elementName' => 'myCustomElement'
            ]
        ]);
        $result = $parser->parse($jsonString);

        $this->assertInstanceOf(StringElement::class, $result);
        $this->assertInternalType('string', $result->getContent()->getValue());
        $this->assertEquals('My custom content', $result->getContent()->getValue());
    }
}
