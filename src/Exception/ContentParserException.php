<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Exception;

class ContentParserException extends \RuntimeException
{

    /**
     * ContentParserException constructor.
     *
     * @param string $elementName
     * @param mixed $providedVariable
     * @param string[] $allowedTypes
     */
    public function __construct(string $elementName, $providedVariable, array $allowedTypes)
    {
        parent::__construct(
            vsprintf(
                '%s : %s is not allowed as content. Allowed types : %s',
                [
                    $elementName,
                    \gettype($providedVariable),
                    implode(' | ', $allowedTypes),
                ]
            )
        );
    }
}
