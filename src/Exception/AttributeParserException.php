<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Exception;

class AttributeParserException extends \RuntimeException
{

    /**
     * AttributeParserException constructor.
     *
     * @param string $elementName
     * @param string $attributeName
     * @param bool $required
     * @param string[] $allowedTypes
     */
    public function __construct(string $elementName, string $attributeName, bool $required, ?array $allowedTypes = null)
    {
        $message = '';

        if ($allowedTypes) {
            $message = vsprintf(
                '%s : %s is a %s attribute. Allowed types %s',
                [
                    $elementName,
                    $attributeName,
                    $required ? 'required' : 'optional',
                    implode(' | ', $allowedTypes),
                ]
            );
        }

        if (!$allowedTypes) {
            $message = vsprintf(
                '%s : %s is a %s attribute.',
                [
                    $elementName,
                    $attributeName,
                    $required ? 'required' : 'optional',
                ]
            );
        }

        parent::__construct($message);
    }
}
