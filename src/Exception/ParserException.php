<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Exception;

class ParserException extends \RuntimeException
{

}
