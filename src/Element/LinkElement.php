<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element;

use Phpro\RefractParser\Element\Primitive\StringElement;
use Phpro\RefractParser\Exception\AttributeParserException;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class LinkElement
 *
 * @package Phpro\RefractParser\Element\Primitive
 */
class LinkElement extends BaseElement
{
    public const ELEMENT = 'link';

    /**
     * LinkElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws ParserException
     * @throws AttributeParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $attributes = $baseElement->getAttributes();

        if (!$attributes->hasAttribute('relation') || !$attributes->getAttribute('relation') instanceof StringElement) {
            throw new AttributeParserException(self::class, 'relation', true, ['StringElement']);
        }

        if (!$attributes->hasAttribute('href') || !$attributes->getAttribute('href') instanceof StringElement) {
            throw new AttributeParserException(self::class, 'href', true, ['StringElement']);
        }

        parent::__construct(
            static::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
