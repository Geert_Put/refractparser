<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Model\Content;

/**
 * Class OptionElement
 *
 * @package Phpro\RefractParser\Element\Primitive
 */
class OptionElement extends BaseElement
{
    public const ELEMENT = 'option';

    /**
     * OptionElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws ParserException
     * @throws ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if (!\is_array($content->getValue())) {
            throw new ContentParserException(self::class, $content->getValue(), ['array']);
        }

        foreach ($content->getValue() as $element) {
            if (!$element instanceof BaseElement) {
                throw new ContentParserException(self::class, $content->getValue(), ['BaseElement[]']);

            }
        }

        parent::__construct(
            static::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
