<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element;

use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Exception\ContentParserException;

/**
 * Class RefElement
 *
 * @package Phpro\RefractParser\Element\Primitive
 */
class RefElement extends BaseElement
{
    public const ELEMENT = 'ref';

    /**
     * RefElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws ParserException
     * @throws ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if (!\is_string($content->getValue())) {
            throw new ContentParserException(self::class, $content->getValue(), ['string']);
        }
        // TODO : implement additional logic to handle the path stuff ( the documentation looks rather iffy )

        parent::__construct(
            static::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
