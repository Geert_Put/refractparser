<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element;

use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class ExtendElement
 *
 * @package Phpro\RefractParser\Element\Primitive
 */
class ExtendElement extends BaseElement
{
    public const ELEMENT = 'extend';

    /**
     * ExtendElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws ParserException
     * @throws ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if (!\is_array($content->getValue())) {
            throw new ContentParserException(self::class, $content->getValue(), ['array']);
        }

        foreach ($content->getValue() as $element) {
            if (!$element instanceof BaseElement) {
                throw new ContentParserException(self::class, $content->getValue(), ['BaseElement[]']);

            }
        }

        parent::__construct(
            static::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
