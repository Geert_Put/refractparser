<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element\Primitive;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class NumberElement
 *
 * @package Phpro\RefractParser\Element\Primitive
 */
class NumberElement extends BaseElement
{
    public const ELEMENT = 'number';

    /**
     * NumberElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws ParserException
     * @throws ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if ($content->getValue() && !(\is_int($content->getValue()) || \is_float($content->getValue()))) {
            throw new ContentParserException(self::class, $content->getValue(), ['int', 'float']);
        }

        parent::__construct(
            static::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
