<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element\Primitive;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Exception\ContentParserException;

/**
 * Class StringElement
 *
 * @package Phpro\RefractParser\Element\Primitive
 */
class StringElement extends BaseElement
{
    public const ELEMENT = 'string';

    /**
     * StringElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws ParserException
     * @throws ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if ($content->getValue() && !\is_string($content->getValue())) {
            throw new ContentParserException(self::class, $content->getValue(), ['string']);
        }

        parent::__construct(
            static::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
