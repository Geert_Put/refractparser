<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element\Primitive;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\RefElement;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class ObjectElement
 *
 * @package Phpro\RefractParser\Element\Primitive
 */
class ObjectElement extends BaseElement
{
    public const ELEMENT = 'object';

    /**
     * ObjectElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws ParserException
     * @throws ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if (!(\is_array($content->getValue()) || $content->getValue() instanceof RefElement || $content->getValue())) {
            throw new ContentParserException(self::class, $content->getValue(), ['array']);
        }

        foreach ($content->getValue() as $element) {
            if (!$element instanceof MemberElement) {
                throw new ContentParserException(self::class, $content->getValue(), ['MemberElement[]']);

            }
        }

        parent::__construct(
            static::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
