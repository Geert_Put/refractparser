<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element\Primitive;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class MemberElement
 *
 * @package Phpro\RefractParser\Element\Primitive
 */
class MemberElement extends BaseElement
{
    public const ELEMENT = 'member';

    /**
     * MemberElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws ParserException
     * @throws ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if (!\is_object($content->getValue())) {
            throw new ContentParserException(self::class, $content->getValue(), ['object']);
        }

        if (!$content->getValue()->key) {
            throw new ParserException(sprintf('%s : The content must be an object with a required \'key\' attribute.', self::class));
        }

        if (!$content->getValue()->key instanceof StringElement) {
            throw new ParserException(sprintf('%s : The content must be an object with a required \'key\' attribute that is a StringElement', self::class));
        }

        if (isset($content->getValue()->value) && !(
                $content->getValue()->value instanceof StringElement
                || $content->getValue()->value instanceof NumberElement
                || $content->getValue()->value instanceof ArrayElement)
        ) {
            throw new ParserException(sprintf('%s : The content must be an object with an optional \'value\' attribute that is a StringElement | NumberElement | ArrayElement', self::class));

        }

        parent::__construct(
            static::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $content
        );
    }
}
