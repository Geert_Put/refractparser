<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element\Primitive;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class ArrayElement
 *
 * @package Phpro\RefractParser\Element\Primitive
 */
class ArrayElement extends BaseElement
{
    public const ELEMENT = 'array';

    /**
     * ArrayElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws ParserException
     * @throws ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if (!\is_array($content->getValue())) {
            throw new ContentParserException(self::class, $content->getValue(), ['array']);
        }

        foreach ($content->getValue() as $element) {
            if (!$element instanceof BaseElement) {
                throw new ContentParserException(self::class, $element, ['BaseElement[]']);

            }
        }

        parent::__construct(
            static::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
