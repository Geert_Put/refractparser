<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element;

use Phpro\RefractParser\Model\Attributes;
use Phpro\RefractParser\Model\Content;
use Phpro\RefractParser\Model\Meta;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Refract Base Element. All Elements should extend from this Class
 *
 * Class BaseElement
 *
 * @package Phpro\RefractParser\Element
 */
class BaseElement
{
    /**
     * @var string
     */
    private $element;

    /**
     * @var Meta
     */
    private $meta;

    /**
     * @var Attributes
     */
    private $attributes;

    /**
     * @var Content
     */
    private $content;

    /**
     * @param string $element
     * @param Meta $meta
     * @param Attributes $attributes
     * @param Content $content
     *
     * @throws ParserException
     */
    public function __construct(
        string $element,
        Meta $meta,
        Attributes $attributes,
        Content $content
    ) {
        if (empty($element)) {
            throw new ParserException(sprintf('%s element must be a non-empty string', self::class));
        }
        $this->element = $element;
        $this->meta = $meta;
        $this->attributes = $attributes;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getElement(): string
    {
        return $this->element;
    }

    /**
     * @return Meta
     */
    public function getMeta(): Meta
    {
        return $this->meta;
    }

    /**
     * @return Attributes
     */
    public function getAttributes(): Attributes
    {
        return $this->attributes;
    }

    /**
     * @return Content
     */
    public function getContent(): Content
    {
        return $this->content;
    }
}
