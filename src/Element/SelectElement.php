<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Element;

use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Exception\ContentParserException;

/**
 * Class SelectElement
 *
 * @package Phpro\RefractParser\Element\Primitive
 */
class SelectElement extends BaseElement
{
    public const ELEMENT = 'select';

    /**
     * SelectElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws ParserException
     * @throws ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if (!\is_array($content->getValue())) {
            throw new ContentParserException(self::class, $content->getValue(), ['array']);
        }

        foreach ($content->getValue() as $element) {
            if (!$element instanceof OptionElement) {
                throw new ContentParserException(self::class, $content->getValue(), ['OptionElement[]']);

            }
        }

        parent::__construct(
            static::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
