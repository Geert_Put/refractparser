<?php

declare(strict_types=1);

namespace Phpro\RefractParser;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\LinkElement;
use Phpro\RefractParser\Element\OptionElement;
use Phpro\RefractParser\Element\Primitive\ArrayElement;
use Phpro\RefractParser\Element\Primitive\BooleanElement;
use Phpro\RefractParser\Element\ExtendElement;
use Phpro\RefractParser\Element\Primitive\MemberElement;
use Phpro\RefractParser\Element\Primitive\NullElement;
use Phpro\RefractParser\Element\Primitive\NumberElement;
use Phpro\RefractParser\Element\Primitive\ObjectElement;
use Phpro\RefractParser\Element\Primitive\StringElement;
use Phpro\RefractParser\Element\RefElement;
use Phpro\RefractParser\Element\SelectElement;
use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Model\Attributes;
use Phpro\RefractParser\Model\Meta;
use Phpro\RefractParser\Model\Content;

/**
 * Class Parser
 *
 * @package Phpro\RefractParser
 */
final class Parser
{
    private const PARSE_ELEMENTS = [
        [
            'className' => StringElement::class,
            'elementName' => StringElement::ELEMENT,
        ],
        [
            'className' => ArrayElement::class,
            'elementName' => ArrayElement::ELEMENT,
        ],
        [
            'className' => BooleanElement::class,
            'elementName' => BooleanElement::ELEMENT,
        ],
        [
            'className' => NullElement::class,
            'elementName' => NullElement::ELEMENT,
        ],
        [
            'className' => NumberElement::class,
            'elementName' => NumberElement::ELEMENT,
        ],
        [
            'className' => ObjectElement::class,
            'elementName' => ObjectElement::ELEMENT,
        ],
        [
            'className' => MemberElement::class,
            'elementName' => MemberElement::ELEMENT,
        ],
        [
            'className' => RefElement::class,
            'elementName' => RefElement::ELEMENT,
        ],
        [
            'className' => LinkElement::class,
            'elementName' => LinkElement::ELEMENT,
        ],
        [
            'className' => ExtendElement::class,
            'elementName' => ExtendElement::ELEMENT,
        ],
        [
            'className' => OptionElement::class,
            'elementName' => OptionElement::ELEMENT,
        ],
        [
            'className' => SelectElement::class,
            'elementName' => SelectElement::ELEMENT,
        ],
    ];

    /**
     * An optional array of custom elements that can also be parsed too
     *
     * @var array
     */
    private $customerParseElements;

    /**
     * Parser constructor.
     *
     * @param array $customerParseElements
     */
    public function __construct(?array $customerParseElements = [])
    {
        foreach ($customerParseElements as $customerParseElement) {
            if (!\is_array($customerParseElement)) {
                throw new ParserException('The custom parse elements array must contain only arrays');
            }

            if (!array_key_exists('className', $customerParseElement) || !\is_string(
                    $customerParseElement['className']
                )) {
                throw new ParserException(
                    'The custom parse elements array must contain arrays with a className key that has a string value'
                );
            }

            if (!array_key_exists('elementName', $customerParseElement) || !\is_string(
                    $customerParseElement['elementName']
                )) {
                throw new ParserException(
                    'The custom parse elements array must contain arrays with a elementName key that has a string value'
                );
            }
        }

        $this->customerParseElements = $customerParseElements;
    }


    /**
     * @param string $json
     *
     * @return BaseElement
     *
     * @throws ParserException
     */
    public function parse(string $json)
    {
        $array = $this->decodeJson($json);

        return $this->parseElement($array);
    }

    /**
     * Decode the json string into an object.
     *
     * @param string $json
     *
     * @return \stdClass
     *
     * @throws ParserException
     */
    private function decodeJson(string $json): \stdClass
    {
        $object = json_decode($json);

        if (!$object instanceof \stdClass) {
            $message = sprintf(
                'Error decoding the json string. json_last_error = %s , json_last_error_msg = %s',
                json_last_error(),
                json_last_error_msg()
            );
            throw new ParserException($message);
        }

        return $object;
    }

    /**
     * Parse a php object to an instance of BaseElement or one of the Refract Elements that extend BaseElement
     *
     * @param \stdClass $object
     *
     * @return BaseElement
     *
     * @throws ParserException
     */
    private function parseElement(\stdClass $object): BaseElement
    {
        $baseElement = $this->parseBaseElement($object);
        $element = $baseElement->getElement();

        foreach (array_merge(self::PARSE_ELEMENTS, $this->customerParseElements) as $parseElement) {
            if ($element === $parseElement['elementName']) {
                return new $parseElement['className']($baseElement);
            }
        }

        return $baseElement;
    }

    /**
     * @param \stdClass $object
     *
     * @return BaseElement
     *
     * @throws ParserException
     */
    private function parseBaseElement(\stdClass $object): BaseElement
    {
        $objectElement = $object->element;
        $objectMeta = $object->meta ?? new \stdClass();
        $objectAttributes = $object->attributes ?? new \stdClass();
        $objectContent = $object->content ?? null;

        $meta = $this->parseMeta($objectMeta);
        $attributes = $this->parseAttributes($objectAttributes);
        $content = $this->parseContent($objectContent);

        return new BaseElement($objectElement, $meta, $attributes, $content);
    }

    /**
     * @param \stdClass $objectAttributes
     *
     * @return Attributes
     *
     * @throws ParserException
     */
    private function parseAttributes(\stdClass $objectAttributes): Attributes
    {
        $attributes = [];
        foreach ((array)$objectAttributes as $objectAttributeKey => $objectAttributeValue) {
            $attributes[$objectAttributeKey] = $this->parseElement($objectAttributeValue);
        }

        return new Attributes($attributes);
    }

    /**
     * @param \stdClass $objectMeta
     *
     * @return Meta
     *
     * @throws ParserException
     */
    private function parseMeta(\stdClass $objectMeta): Meta
    {
        $objectMetaId = $objectMeta->id ?? null;
        $objectMetaRef = $objectMeta->ref ?? null;
        $objectMetaClasses = $objectMeta->classes ?? null;
        $objectMetaTitle = $objectMeta->title ?? null;
        $objectMetaDescription = $objectMeta->description ?? null;
        $objectMetaLinks = $objectMeta->links ?? null;

        $metaId = $objectMetaId ? $this->parseElement($objectMetaId) : null;
        $metaRef = $objectMetaRef ? $this->parseElement($objectMetaRef) : null;
        $metaClasses = $objectMetaClasses ? $this->parseElement($objectMetaClasses) : null;
        $metaTitle = $objectMetaTitle ? $this->parseElement($objectMetaTitle) : null;
        $metaDescription = $objectMetaDescription ? $this->parseElement($objectMetaDescription) : null;
        $metaLinks = $objectMetaLinks ? $this->parseElement($objectMetaLinks) : null;

        return new Meta($metaId, $metaRef, $metaClasses, $metaTitle, $metaDescription, $metaLinks);
    }

    /**
     * @param mixed $objectContent
     *
     * @return Content
     *
     * @throws ParserException
     */
    private function parseContent($objectContent): Content
    {
        if ($objectContent === null) {
            return new Content();
        }

        if ($objectContent instanceof \stdClass) {
            $content = new \stdClass();
            if (isset($objectContent->element)) {
                $content = $this->parseElement($objectContent);
            }
            if (isset($objectContent->key)) {
                $content->key = $this->parseElement($objectContent->key);
            }
            if (isset($objectContent->value)) {
                $content->value = $this->parseElement($objectContent->value);
            }

            return new Content($content);
        }

        if (\is_array($objectContent)) {
            $content = [];
            foreach ($objectContent as $element) {
                $content[] = $this->parseElement($element);
            }

            return new Content($content);
        }

        return new Content($objectContent);
    }
}
