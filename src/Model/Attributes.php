<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Model;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class Attributes
 *
 * @package Phpro\RefractParser\Model
 */
final class Attributes
{

    /**
     * @var BaseElement[]
     */
    private $attributes;

    /**
     * Attributes constructor.
     *
     * @param BaseElement[] $attributes
     *
     * @throws ParserException
     */
    public function __construct(array $attributes)
    {
        foreach ($attributes as $attribute) {
            if (!$attribute instanceof BaseElement) {
                throw new ParserException(
                    sprintf(
                        '%s : %s is not an allowed attribute value. Allowed types : BaseElement',
                        self::class,
                        \gettype($attribute)
                    )
                );
            }
        }
        $this->attributes = $attributes;
    }

    /**
     * @param string $name
     *
     * @return BaseElement|null
     *
     * @throws ParserException
     */
    public function getAttribute(string $name): ?baseElement
    {
        if (!array_key_exists($name, $this->attributes)) {
            throw new ParserException(
                sprintf('%s : Attribute %s does not exist.', self::class, $name)
            );
        }

        return $this->attributes[$name];
    }

    /**
     * @param string $name
     *
     * @return bool
     *
     * @throws ParserException
     */
    public function hasAttribute(string $name): bool
    {
        if (array_key_exists($name, $this->attributes)) {
            return true;
        }

        return false;
    }

    /**
     * @return array|BaseElement[]
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }
}
