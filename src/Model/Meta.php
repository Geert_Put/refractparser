<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Model;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\LinkElement;
use Phpro\RefractParser\Element\Primitive\ArrayElement;
use Phpro\RefractParser\Element\Primitive\StringElement;
use Phpro\RefractParser\Element\RefElement;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class Meta
 *
 * @package Phpro\RefractParser\Model
 */
final class Meta
{
    /**
     * @var BaseElement
     */
    private $id;

    /**
     * @var RefElement
     */
    private $ref;

    /**
     * @var ArrayElement
     */
    private $classes;

    /**
     * @var StringElement
     */
    private $title;

    /**
     * @var StringElement
     */
    private $description;

    /**
     * @var ArrayElement
     */
    private $links;

    /**
     * Meta constructor.
     * @param StringElement $id
     * @param RefElement $ref
     * @param ArrayElement $classes
     * @param StringElement $title
     * @param StringElement $description
     * @param ArrayElement $links
     *
     * @throws ParserException
     */
    public function __construct(
        ?StringElement $id = null,
        ?RefElement $ref = null,
        ?ArrayElement $classes = null,
        ?StringElement $title = null,
        ?StringElement $description = null,
        ?ArrayElement $links = null
    ) {
        $this->id = $id;
        $this->ref = $ref;

        if ($classes) {
            foreach ($classes->getContent()->getValue() as $class) {
                if (!$class instanceof StringElement) {
                    throw new ParserException(
                        sprintf('%s : The meta property \'classes\' must be of the type StringElement[]', self::class)
                    );
                }
            }
        }

        $this->classes = $classes;
        $this->title = $title;
        $this->description = $description;

        if ($links) {
            foreach ($links->getContent()->getValue() as $link) {
                if (!$link instanceof LinkElement) {
                    throw new ParserException(
                        sprintf('%s : The meta property \'links\' must be of the type LinkElement[]', self::class)
                    );
                }
            }
        }

        $this->links = $links;
    }

    /**
     * @return BaseElement|null
     */
    public function getId(): ?BaseElement
    {
        return $this->id;
    }

    /**
     * @return RefElement|null
     */
    public function getRef(): ?RefElement
    {
        return $this->ref;
    }

    /**
     * @return ArrayElement|null
     */
    public function getClasses(): ?ArrayElement
    {
        return $this->classes;
    }

    /**
     * @return StringElement|null
     */
    public function getTitle(): ?StringElement
    {
        return $this->title;
    }

    /**
     * @return StringElement|null
     */
    public function getDescription(): ?StringElement
    {
        return $this->description;
    }

    /**
     * @return ArrayElement|null
     */
    public function getLinks(): ?ArrayElement
    {
        return $this->links;
    }
}
