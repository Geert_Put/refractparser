<?php

declare(strict_types=1);

namespace Phpro\RefractParser\Model;

/**
 * Class Content
 *
 * @package Phpro\RefractParser\Model
 */
final class Content
{
    /**
     * @var mixed
     */
    private $content;

    /**
     * Content constructor.
     *
     * @param $content
     */
    public function __construct($content = null)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->content;
    }
}
